using System;
using System.Drawing;
using System.Windows.Forms;
using System.Net.Http;
using DiscordInterface;
using Utils;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;

namespace MusicBeePlugin
{
	public partial class Plugin
	{
		private PluginInfo about = new PluginInfo();

		private static DiscordRPC.RichPresence rpc_presence = new DiscordRPC.RichPresence();

		public static MusicBeeApiInterface mbApiInterface;

		public static Logger logging = new Logger();
		
		public static readonly HttpClient http_client = new HttpClient();

		public static string discord_id = "Your Discord APP ID";


		public PluginInfo Initialise(IntPtr apiInterfacePtr)
		{
			mbApiInterface = new MusicBeeApiInterface();
			mbApiInterface.Initialise(apiInterfacePtr);
			about.PluginInfoVersion = PluginInfoVersion;
			about.Name = "Discord Rich Presence";
			about.Description = "A Richer Rich Presence for Musicbee.";
			about.Author = "@cleaninfla";
			about.TargetApplication = "";   
			about.Type = PluginType.General;
			about.VersionMajor = 1;  
			about.VersionMinor = 0;
			about.Revision = 01; 
			about.ReceiveNotifications = (ReceiveNotificationFlags.PlayerEvents | ReceiveNotificationFlags.TagEvents);
			about.ConfigurationPanelHeight = 0;

			http_client.DefaultRequestHeaders.Add("Authorization", "Your Authorization Key");

			InitialiseDiscord();

			return about;
		}

		private void InitialiseDiscord()
		{
			DiscordRPC.DiscordEventHandlers handlers = new DiscordRPC.DiscordEventHandlers();

			handlers.readyCallback = HandleReadyCallback;
			handlers.errorCallback = HandleErrorCallback;
			handlers.disconnectedCallback = HandleDisconnectedCallback;
			
			DiscordRPC.Initialize(discord_id, ref handlers, true, null);
		}

		private void HandleReadyCallback() { }
		private void HandleErrorCallback(int errorCode, string message) { }
		private void HandleDisconnectedCallback(int errorCode, string message) { }

		private async void ProcessArtwork(string album)
		{
			string asset_list = await Discord.GetAssetList().ReadAsStringAsync();

			string artwork_data = mbApiInterface.NowPlaying_GetArtwork();

			string data_uri = "data:image/png;base64," + artwork_data;

			if (asset_list.Contains(album))
			{
				rpc_presence.largeImageKey = album;
				return;
			}
			else
			{
				rpc_presence.largeImageKey = "temp_uploading";
				await Discord.UploadAsset(album, data_uri);
				rpc_presence.largeImageKey = album;
				return;
			}
		}

		private void UpdatePresence(string artist, string track_artist, string track, string album, string duration, Boolean playing, int position, int volume, bool handle_artworks = false)
		{
			track = Utility.Utf16ToUtf8(track + " ");
			artist = Utility.Utf16ToUtf8("by " + artist);
			track_artist = Utility.Utf16ToUtf8("by " + track_artist);

			rpc_presence.state = track_artist.Substring(0, track_artist.Length - 1);
			rpc_presence.details = track.Substring(0, track.Length - 1);

			string large_text = " ";

			if (string.IsNullOrEmpty(album))
				large_text = track + " " + track_artist;
			else
				large_text = album + " " + artist;

			rpc_presence.largeImageText = large_text.Substring(0, large_text.Length - 1);

			char[] albumArray = album.ToCharArray();

			Regex symbol_pattern = new Regex(@"[!@#$%^&*()+=\'[\""{\]};:<>|./?,\s-]", RegexOptions.Compiled);

			foreach (Match m in symbol_pattern.Matches(album))
				albumArray[m.Index] = '_';

			string new_album = new String(albumArray).ToLower();

			if (handle_artworks)
				ProcessArtwork(new_album);

			long now = (long)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

			if (playing)
			{
				rpc_presence.startTimestamp = now - position;

				string[] durations = duration.Split(':');

				long end = System.Convert.ToInt64(durations[0]) * 60 + System.Convert.ToInt64(durations[1]);

				rpc_presence.endTimestamp = rpc_presence.startTimestamp + end;

				rpc_presence.smallImageKey = "playing";
				rpc_presence.smallImageText = "Playing at " + volume.ToString() + "%";
			}

			else
			{
				rpc_presence.endTimestamp = 0;
				rpc_presence.startTimestamp = 0;
				rpc_presence.smallImageKey = "paused";
				rpc_presence.smallImageText = "Paused";
			}

			DiscordRPC.UpdatePresence(ref rpc_presence);
		}

		public void Close(PluginCloseReason reason)
		{
			DiscordRPC.Shutdown();
		}

		public void ReceiveNotification(string sourceFileUrl, NotificationType type)
		{
			string artist = mbApiInterface.NowPlaying_GetFileTag(MetaDataType.AlbumArtist);
			string track_artist = mbApiInterface.NowPlaying_GetFileTag(MetaDataType.Artist);
			string track_title = mbApiInterface.NowPlaying_GetFileTag(MetaDataType.TrackTitle);
			string album = mbApiInterface.NowPlaying_GetFileTag(MetaDataType.Album);
			string duration = mbApiInterface.NowPlaying_GetFileProperty(FilePropertyType.Duration);
			int position = mbApiInterface.Player_GetPosition();
			int volume = Convert.ToInt32(mbApiInterface.Player_GetVolume() * 100.0f);

			if (string.IsNullOrEmpty(artist)) { artist = "[artist empty]"; }

			switch (type)
			{
				case NotificationType.PlayStateChanged:
					switch (mbApiInterface.Player_GetPlayState())
					{
						case PlayState.Playing:
							UpdatePresence(artist, track_artist, track_title, album, duration, true, position / 1000, volume);
							break;
						case PlayState.Paused:
							UpdatePresence(artist, track_artist, track_title, album, duration, false, 0, volume);
							break;
					}
					break;
				case NotificationType.TrackChanged:
					UpdatePresence(artist, track_artist, track_title, album, duration, true, 0, volume, true);
					break;
				case NotificationType.VolumeLevelChanged:
					switch (mbApiInterface.Player_GetPlayState())
					{
						case PlayState.Playing:
							UpdatePresence(artist, track_artist, track_title, album, duration, true, position / 1000, volume);
							break;
					}
					break;
			}
		}
	}
}
